#!/usr/bin/python3
# based on
# github.com/sichkar-valentyn
from typing import List, Tuple

import numpy as np

from utils import plots, worldmap_utils as maps, io_utils
from domain import FieldType, Move
from qlearn.agent_brain import QLearningTable
from qlearn.tk_env import Environment

STEP_INDEX = 0
REWARD_INDEX = 1
COST_INDEX = 2


def generate_occurrence_heatmap(world_map: np.ndarray,
                                env: Environment,
                                brain: QLearningTable,
                                start_pos: Tuple[int, int],
                                monster_pos: Tuple[int, int],
                                epochs: int,
                                epoch_size: int,
                                generate: bool,
                                title: str
                                ):
    heatmap = np.zeros(world_map.shape, dtype=float)
    # Resulted list for the plotting Episodes via Steps

    # Summed costs for all episodes in resulted list
    stats_per_epoch: List[np.ndarray] = []

    step_count = 0

    for epoch_n in range(epochs):

        epoch_stats = np.zeros((3, epoch_size))

        for episode_in_epoch in range(epoch_size):

            print("epoch : {}, episode {}".format(epoch_n, episode_in_epoch))
            # Initial Observation
            # todo - i can get current route here

            observation = env.reset()

            i = 0

            action = brain.choose_action(io_utils.tokenize_state(observation))
            while True:

                # brain chooses action based on observation
                if brain.is_sarsa:
                    observation_, reward, done = env.step(action)
                    action_ = brain.choose_action(io_utils.tokenize_state(observation_))
                    if generate:
                        brain.learn(io_utils.tokenize_state(observation), action, reward,
                                    io_utils.tokenize_state(observation_), action_)
                    action = action_
                else:
                    action = brain.choose_action(io_utils.tokenize_state(observation))
                    observation_, reward, done = env.step(action)
                    if generate:
                        brain.learn(io_utils.tokenize_state(observation), action, reward,
                                    io_utils.tokenize_state(observation_), None)

                if type(observation_) != str:
                    heatmap[observation_[0], observation_[1]] += 1

                # Swapping the observations - current and next
                observation = observation_

                # Calculating number of Steps in the current Episode
                i += 1

                # Break while loop when it is the end of current Episode
                # When agent reached the goal or obstacle
                if done:
                    # plot
                    break
            step_count += i
        stats_per_epoch.append(epoch_stats)

    # Showing the final route
    env.final()

    brain.print_q_table()

    if generate:
        brain.save()
    else:
        world_map[start_pos[0], start_pos[1]] = FieldType.PLAYER.value
        heatmap[start_pos[0], start_pos[1]] = 0
        heatmap[monster_pos[0], monster_pos[1]] = 0

        # map_with_heatmap = world_map + heatmap
        # plt.imshow(world_map)
        # plt.title("wobraind")
        # plt.show()
        # plt.imshow(heatmap)
        # plt.title("heat")
        # plt.show()
        # plt.imshow(heatmap / step_count)
        # plt.title("heat normalized")
        # plt.show()
        # plt.imshow(map_with_heatmap)
        # plt.title("both")
        # plt.show()

        plots.plot_heatmap(world_map, heatmap, start_pos, title)


def plot_moves():
    world_map, start_pos, monster_pos = io_utils.load_map_start_monster()
    brain = QLearningTable(actions=[move for move in Move])

    world_map[start_pos[0], start_pos[1]] = FieldType.PLAYER.value

    brain.load()

    moves = [m for m in Move] + [None]

    for m in moves:
        plots.plot_direction_move(world_map, brain, start_pos, m, "")


# Commands to be implemented after running this file
def main(title: str):
    # Calling for the environment
    generate = True
    learned = True

    if generate:
        world_map = maps.worldmap_without_unreachable_places((16, 16),
                                                             {FieldType.EMPTY: 20, FieldType.MOUNTAIN: 30})
        monster_pos = maps.random_pos_on_empty_field(world_map)
        world_map[monster_pos[0], monster_pos[1]] = FieldType.MONSTER.value
        start_pos = maps.random_pos_on_empty_field(world_map)
        # world_map, start_pos, monster_pos = map_start_monster()

        env = Environment(world_map, start_pos, step_limit=700)
        # Calling for the main algorithm
        brain = QLearningTable(actions=[move for move in Move], title=title)
        # Running the main loop with Episodes by calling the function update()
        generate_occurrence_heatmap(epochs=10, epoch_size=100, generate=True, brain=brain, env=env,
                                    monster_pos=monster_pos,
                                    start_pos=start_pos,
                                    title=title, world_map=world_map)

    else:
        world_map, start_pos, monster_pos = io_utils.load_map_start_monster()
        brain = QLearningTable(actions=[move for move in Move], title=title)

        if learned:
            brain.load()

        env = Environment(world_map, start_pos, step_limit=700)
        generate_occurrence_heatmap(epochs=5, epoch_size=100, generate=False, brain=brain, env=env,
                                    monster_pos=monster_pos,
                                    start_pos=start_pos,
                                    title=title, world_map=world_map)

    if __name__ == '__main__':
        # main()
        plot_moves()
