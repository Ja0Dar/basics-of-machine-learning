#!/usr/bin/python3
# based on
# github.com/sichkar-valentyn
from typing import List, Tuple

from utils import animate_raw, plots, io_utils
import numpy as np
from qlearn.agent_brain import QLearningTable
from qlearn.tk_env import Environment

STEP_INDEX = 0
REWARD_INDEX = 1
COST_INDEX = 2


def reward_plots(epochs: int, epoch_size: int, env: Environment, RL: QLearningTable, world_map: np.ndarray,
                 start_pos: Tuple[int, int], animate: bool = False):
    # Resulted list for the plotting Episodes via Steps
    steps = []

    # Summed costs for all episodes in resulted list
    stats_per_epoch: List[np.ndarray] = []
    all_costs = []
    all_rewards = []
    mmap = None

    for epoch_n in range(epochs):

        epoch_stats = np.zeros((5, epoch_size))

        for episode_in_epoch in range(epoch_size):

            print("epoch : {}, episode {}".format(epoch_n, episode_in_epoch))
            if animate:
                mmap = animate_raw.MapPrinter(None, world_map, initial_pos=start_pos)

            observation = env.reset()

            i = 0

            cost = 0
            episode_reward = 0

            action = RL.choose_action(io_utils.tokenize_state(observation))

            while True:

                if RL.is_sarsa:
                    observation_, reward, done = env.step(action)
                    action_ = RL.choose_action(io_utils.tokenize_state(observation_))
                    cost += RL.learn(io_utils.tokenize_state(observation), action, reward,
                                     io_utils.tokenize_state(observation_), action_)
                    action = action_
                else:
                    action = RL.choose_action(io_utils.tokenize_state(observation))
                    observation_, reward, done = env.step(action)
                    cost += RL.learn(io_utils.tokenize_state(observation), action, reward,
                                     io_utils.tokenize_state(observation_), None)

                # RL takes an action and get the next observation and reward
                if mmap is not None and type(observation_) != str:
                    y, x = observation_
                    mmap.update_pos(y, x)
                    mmap.render()

                # RL learns from this transition and calculating the cost

                episode_reward += reward

                # Swapping the observations/actions - current and next
                observation = observation_

                # Calculating number of Steps in the current Episode
                i += 1

                # Break while loop when it is the end of current Episode
                # When agent reached the goal or obstacle
                if done:
                    if animate:
                        mmap.remove_player()
                    epoch_stats[STEP_INDEX, episode_in_epoch] = i
                    epoch_stats[REWARD_INDEX, episode_in_epoch] = episode_reward
                    epoch_stats[COST_INDEX, episode_in_epoch] = cost
                    steps += [i]
                    all_costs += [cost]
                    all_rewards += [episode_reward]
                    break
            if mmap is not None:
                mmap.finalize()

        stats_per_epoch.append(epoch_stats)

    env.final()
    RL.save()
    RL.print_q_table()
    RL.plot_results(steps, all_costs, all_rewards)
    plots.plot_epochs_stats(stats_per_epoch)

