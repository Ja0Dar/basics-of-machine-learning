# based on github.com/sichkar-valentyn
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from functional import seq

from utils import io_utils
from qlearn.tk_env import final_states


class QLearningTable:
    def __init__(self, actions, title: str, alpha=0.01, gamma=0.9, epsilon=0.9, is_sarsa: bool = False):
        self.is_sarsa = is_sarsa
        self.title = title
        self.actions = actions
        self.learning_rate = alpha
        self.gamma = gamma
        self.epsilon = epsilon

        self.q_table: pd.DataFrame = pd.DataFrame(columns=self.actions, dtype=np.float64)
        # Creating Q-table for cells of the final route
        self.q_table_final: pd.DataFrame = pd.DataFrame(columns=self.actions, dtype=np.float64)

    def choose_action(self, observation):
        self.check_state_exist(observation)
        if np.random.uniform() < self.epsilon:
            state_action = self.q_table.loc[observation, :]
            state_action = state_action.reindex(np.random.permutation(state_action.index))
            action = state_action.idxmax()
        else:
            action = np.random.choice(self.actions)
        return action

    # Function for learning and updating Q-table with new knowledge
    def learn(self, state, action, reward, next_state, next_action):
        self.check_state_exist(next_state)

        q_predict = self.q_table.loc[state, action]

        if (next_action is None) == self.is_sarsa:
            raise Exception("Next action should be defined only in sarsa")

        if next_state != 'goal' or next_state != 'obstacle':
            if next_action is None:
                q_target = reward + self.gamma * self.q_table.loc[next_state, :].max()
            else:
                q_target = reward + self.gamma * self.q_table.loc[next_state, next_action]
        else:
            q_target = reward

        self.q_table.loc[state, action] += self.learning_rate * (q_target - q_predict)
        return self.q_table.loc[state, action]

    # Adding to the Q-table new states
    def check_state_exist(self, state):
        if state not in self.q_table.index:
            self.q_table = self.q_table.append(
                pd.Series(
                    [0] * len(self.actions),
                    index=self.q_table.columns,
                    name=state,
                )
            )

    # Printing the Q-table with states
    def print_q_table(self):
        # Getting the coordinates of final route from tk_env.py
        e = final_states()

        # Comparing the indexes with coordinates and writing in the new Q-table values
        for i in range(len(e)):
            state = io_utils.tokenize_state(e[i])  # state = '[5.0, 40.0]'
            # Going through all indexes and checking
            for j in range(len(self.q_table.index)):
                if self.q_table.index[j] == state:
                    self.q_table_final.loc[state, :] = self.q_table.loc[state, :]

        print()
        print('Length of final Q-table =', len(self.q_table_final.index))
        print('Final Q-table with values from the final route:')
        print(self.q_table_final)

        print()
        print('Length of full Q-table =', len(self.q_table.index))
        print('Full Q-table:')
        print(self.q_table)

    # Plotting the results for the number of steps
    def save(self):
        self.q_table.to_csv("tmp/q_table.csv")
        self.q_table_final.to_csv("tmp/q_table_final.csv")
        io_utils.dump(self.q_table, "q_table")
        io_utils.dump(self.q_table_final, "q_table_final")
        with open("tmp/rates", "w") as file:
            file.write(";".join(map(lambda s: str(s), [self.learning_rate, self.gamma, self.epsilon])))

    def load(self):
        self.q_table = io_utils.load("q_table")
        self.q_table_final = io_utils.load("q_table_final")

        with open("tmp/rates", "r") as file:
            line = seq(file.readlines()[0].split(";")).map(lambda x: float(x)).to_list()
            [self.learning_rate, self.gamma, self.epsilon] = line

    def plot_results(self, steps, cost, reward):

        plt.figure()
        plt.plot(np.arange(len(steps)), steps, 'b')
        plt.title('{}: Episode via steps'.format(self.title))
        plt.xlabel('Episode')
        plt.ylabel('Steps')
        plt.savefig("tmp/jpegs/episode_steps.jpg")
        plt.show()

        plt.figure()
        plt.plot(np.arange(len(cost)), cost, 'r')
        plt.title('{}: Episode via cost'.format(self.title))
        plt.xlabel('Episode')
        plt.ylabel('Cost')
        plt.savefig("tmp/jpegs/episode_costs.jpg")
        plt.show()

        plt.figure()
        plt.plot(np.arange(len(reward)), reward, 'g')
        plt.xlabel('Episode')
        plt.ylabel('Reward')
        plt.title('{}: Episode via reward'.format(self.title))
        plt.savefig("tmp/jpegs/episode_rewards.jpg")
        plt.show()
