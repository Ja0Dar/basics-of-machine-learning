#!/usr/bin/python3
# based on
# github.com/sichkar-valentyn
import copy
from typing import Tuple, Dict

import numpy as np

from domain import Move, FieldType

# Global variable for dictionary with coordinates for the final route
a = {}  # todo- fix?


class Environment:
    def __init__(self, world_map: np.ndarray, initial_agent_pos: Tuple[int, int], monster_pos: Tuple[int, int],
                 step_limit: int):
        self.monster_pos = monster_pos
        self.step_limit = step_limit
        self.action_space = [move for move in Move]
        self.n_actions = len(self.action_space)
        self.world_map = world_map

        self.current_route: Dict[int, Tuple[int, int]] = {}
        self.final_route = {}

        # Key for the dictionaries
        self.i = 0

        self.didnt_win_yet = True

        self.longest_path_len = 0

        self.first_route = {}
        self.shortest_path_len = 0
        self.initial_agent_pos = np.array(initial_agent_pos)
        self.agent_pos = copy.deepcopy(initial_agent_pos)
        self.friend_found = False

    # reset the environment and start new Episode
    def reset(self):
        print("reset at step {}".format(self.i))
        self.agent_pos = self.initial_agent_pos
        self.current_route = {}
        self.i = 0
        return self.agent_pos

    def step(self, action: Move):
        delta_pos = np.array({
                                 Move.UP: (-1, 0),
                                 Move.RIGHT: (0, 1),
                                 Move.DOWN: (1, 0),
                                 Move.LEFT: (0, -1)
                             }[action])

        maybe_new_pos = self.agent_pos + delta_pos

        maybe_new_pos2 = np.array([
            np.clip(maybe_new_pos[0], 0, self.world_map.shape[0] - 1),
            np.clip(maybe_new_pos[1], 0, self.world_map.shape[1] - 1),
        ])

        if self.world_map[maybe_new_pos2[0], maybe_new_pos2[1]] != FieldType.MOUNTAIN.value:
            self.agent_pos = maybe_new_pos2
        self.current_route[self.i] = self.agent_pos

        witchers_field: FieldType = self.world_map[self.agent_pos[0], self.agent_pos[1]]

        # Updating key for the dictionary
        self.i += 1

        if self.i == self.step_limit or witchers_field == FieldType.LAVA.value:
            reward = -100
            next_state = 'obstacle'
            done = True
            pass
        elif witchers_field == FieldType.MONSTER.value:
            reward = 100
            done = True
            next_state = 'goal'

            if self.didnt_win_yet:
                self.first_route = copy.deepcopy(self.current_route)
                self.final_route = copy.deepcopy(self.current_route)
                self.didnt_win_yet = False
                self.longest_path_len = len(self.current_route)
                self.shortest_path_len = len(self.current_route)

            if len(self.current_route) < len(self.final_route):
                self.shortest_path_len = len(self.current_route)
                self.final_route = copy.deepcopy(self.current_route)

            if len(self.current_route) > self.longest_path_len:
                self.longest_path_len = len(self.current_route)

        elif witchers_field == FieldType.EMPTY.value or witchers_field == FieldType.PLAYER.value:
            reward = -1
            next_state = self.agent_pos
            done = False

        elif witchers_field == FieldType.TRISS.value:
            # todo:bcm - generate triss near witcher
            self.friend_found = True
            near = 2
            teleported = False
            for y in range(self.monster_pos[0] - near, self.monster_pos[0] + near, ):
                for x in range(self.monster_pos[1] - near, self.monster_pos[1] + near, ):
                    if self.world_map[y, x] == FieldType.EMPTY.value:
                        teleported = True
                        next_state = np.array((y, x))
                        break
            if not teleported:
                print("TRISS: No space available near monster")
                next_state = self.agent_pos
            reward = -1
            done = False
        elif witchers_field == FieldType.SHANI.value:
            if self.friend_found:
                reward = -1
            else:
                reward = 30
            next_state = self.agent_pos
            done = False

        else:
            raise Exception(
                "Not expected fieldId {}, field: {}".format(witchers_field, FieldType.from_value(witchers_field)))

        return next_state, reward, done

    # Function to show the found route
    def final(self):
        # Deleting the agent at the end
        # Showing the number of steps
        print('The shortest route:', self.shortest_path_len)
        print('The longest route:', self.longest_path_len)

        # Creating initial point
        # todo:bcm - plot first route,
        # todo: plot final routre

        # Filling the route
        for j in range(len(self.final_route)):
            # Showing the coordinates of the final route
            print(self.final_route[j])
            # todo - show step
            # Writing the final route in the global variable a
            a[j] = self.final_route[j]


def final_states():
    return a
