#!/usr/bin/env python3
from typing import Tuple

import numpy as np
from matplotlib import pyplot as plt

from domain import FieldType


class MapPrinter:
    def __init__(self, fig_ax, worldmap: np.ndarray, initial_pos: Tuple[int, int]):
        self.prev_field_value = FieldType.EMPTY.value
        self.current_pos = initial_pos
        self.worldmap = worldmap
        self.fig, self.ax = plt.subplots(1, 1) if fig_ax is None else fig_ax
        self.ax.set_aspect('equal')
        self.ax.set_xlim(0, worldmap.shape[1])
        self.ax.set_ylim(0, worldmap.shape[0])
        plt.show(False)
        plt.draw()
        self.img = plt.imshow(self.worldmap)

    def update_pos(self, y: int, x: int):
        self.worldmap[self.current_pos[0], self.current_pos[1]] = self.prev_field_value
        self.prev_field_value = self.worldmap[y, x]
        self.current_pos = y, x
        self.worldmap[y, x] = FieldType.PLAYER.value

    def remove_player(self):
        self.worldmap[self.current_pos[0], self.current_pos[1]] = self.prev_field_value

    def render(self):
        self.img.set_data(self.worldmap)

        # trying blitting
        # self.fig.canvas.restore_region(self.background)
        # self.ax.draw_artist(self.img)
        # self.fig.canvas.blit(self.ax.bbox)
        self.fig.canvas.draw()

    def finalize(self):
        plt.close(self.fig)

