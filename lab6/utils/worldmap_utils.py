import random
from copy import deepcopy
from typing import Dict, List, Tuple, Optional

import numpy as np
from functional import seq

from domain import Field, FieldType


def __dist_sqr(y1, x1, y2, x2):
    yd = y1 - y2
    xd = x1 - x2
    return xd * xd + yd * yd


def generate_world_map(shape: Tuple[int, int], counts: Dict[FieldType, int]) -> np.ndarray:
    def random_coord(ind: int) -> int:
        return random.randint(0, shape[ind] - 1)

    cluster_centers: List[Field] = []
    world_map = np.ones(shape, dtype=int) * -1

    for field_type, center_count in counts.items():
        for _ in range(center_count):
            field = Field(random_coord(1), random_coord(0), field_type)
            if world_map[field.y, field.x] == -1:
                cluster_centers.append(field)
                world_map[field.y, field.x] = field_type.value

    cluster_centers = seq(cluster_centers)

    for x in range(shape[1]):
        for y in range(shape[0]):
            if world_map[y, x] == -1:
                field_type = cluster_centers.min_by(lambda center: __dist_sqr(center.y, center.x, y, x)).field_type
                world_map[y, x] = field_type.value
    return world_map


def __nghbs(y, x, shape):
    return filter(lambda a: 0 <= a[0] < shape[0] and 0 <= a[1] < shape[1],
                  (  # (y - 1, x - 1),
                      (y, x - 1),
                      # (y + 1, x - 1),
                      (y - 1, x),
                      (y + 1, x),
                      # (y - 1, x + 1),
                      (y, x + 1)))
    # (y + 1, x + 1)))


# todo:bcm - it might not work - fix it
def unreachable_places(map_: np.ndarray):
    c_map = deepcopy(map_)

    def is_empty(y, x):
        return c_map[y, x] % 10 == FieldType.EMPTY.value

    empty_cluster_ids = []
    current_empty_identifier = 10 + FieldType.EMPTY.value

    # assumption: y,x is EMPTY
    def go_deeper(y, x) -> List[Tuple[int, int]]:
        if is_empty(y, x) and c_map[y, x] != FieldType.EMPTY.value:
            return []
        c_map[y, x] = current_empty_identifier
        return [(y_, x_) for y_, x_ in __nghbs(y, x, c_map.shape) if c_map[y_, x_] == FieldType.EMPTY.value]

    # todo: bcm - not sure why do i nedd 0 instrad of last_y / x
    def find_not_marked_empty_point(last_y, last_x) -> Optional[Tuple[int, int]]:
        for y in range(0, c_map.shape[0]):
            for x in range(0, c_map.shape[1]):
                if c_map[y, x] == FieldType.EMPTY.value:
                    return y, x
        return None

    point_tup = find_not_marked_empty_point(0, 0)

    while point_tup is not None:
        row, col = point_tup

        scanned_list = seq(go_deeper(row, col))

        while not scanned_list.empty():
            tmp = scanned_list.flat_map(lambda tup: go_deeper(tup[0], tup[1]))
            scanned_list = tmp

        empty_cluster_ids.append(current_empty_identifier)
        current_empty_identifier += 10

        point_tup = find_not_marked_empty_point(row, col)
    return c_map, empty_cluster_ids


def has_unreachable_spots(map_: np.ndarray):
    new_word_map, cids = unreachable_places(map_)
    return len(cids) != 1


def worldmap_without_unreachable_places(shape: Tuple[int, int], c: Dict[FieldType, int]):
    world_map = generate_world_map(shape, c)
    while has_unreachable_spots(world_map):
        world_map = generate_world_map(shape, c)
    return world_map


def random_pos_on_empty_field(mmap: np.ndarray) -> Tuple[int, int]:
    def random_coords():
        y_, x_ = np.random.sample(2) * np.array(mmap.shape)
        return int(y_), int(x_)

    y, x = random_coords()
    while mmap[y, x] != FieldType.EMPTY.value:
        y, x = random_coords()
    return y, x
