from typing import List, Tuple, Optional
from matplotlib import colors as mcolors

from utils import io_utils
from domain import Move, FieldType
from qlearn.agent_brain import QLearningTable
from run_agent import STEP_INDEX, REWARD_INDEX
import matplotlib.patches as mpatches
from functional import seq
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


def plot_epochs_stats(epochs_stats: List[np.ndarray]):
    epochs = seq(epochs_stats)

    def grouped_stats(index: int):
        return epochs.map(lambda epoch_stats: epoch_stats[index, :].sum())

    steps = grouped_stats(STEP_INDEX).to_list()
    plt.figure()
    plt.plot(np.arange(len(steps)), steps, 'b')
    plt.title('Epoch via step sum')
    plt.xlabel('Epoch')
    plt.ylabel('Step sum')
    plt.savefig("tmp/jpegs/epochs_step_sum.jpg")
    plt.show()

    reward = grouped_stats(REWARD_INDEX).to_list()
    plt.figure()
    plt.plot(np.arange(len(reward)), reward, 'g')
    plt.xlabel('Epoch')
    plt.ylabel('Reward sum')
    plt.title('Epoch via reward sum')
    plt.savefig("tmp/jpegs/epochs_step_reward_sum.jpg")
    plt.show()

    mean_rewards = epochs.map(lambda epoch_stats: epoch_stats[REWARD_INDEX, :].mean()).to_list()
    std_rewards = epochs.map(lambda epoch_stats: epoch_stats[REWARD_INDEX, :].std()).to_list()
    plt.figure()
    plt.errorbar(np.arange(len(reward)), mean_rewards, std_rewards, fmt='.')
    plt.xlabel('Epoch')
    plt.ylabel('Average reward')
    plt.title('Average reward per epoch')
    plt.savefig("tmp/jpegs/epochs_avg_reward.jpg")
    plt.show()


def plot_direction_move(world_map: np.ndarray, brain: QLearningTable, start_pos: Tuple[int, int], move: Optional[Move],
                        title_prefix: str):
    h, w = world_map.shape
    # todo:bcm - why

    q_tab: pd.DataFrame = brain.q_table

    q_heatmap = np.zeros(world_map.shape)

    worst_score: int = q_tab.values.min()
    abs_worst = abs(worst_score)

    for y in range(h):
        for x in range(w):
            pos_token = io_utils.tokenize_state((y, x))

            if move is None:
                qmax = q_tab.loc[pos_token, :].max() if q_tab.index.contains(pos_token) else worst_score

                q_heatmap[y, x] = qmax + abs_worst
            else:
                q_heatmap[y, x] = (q_tab.loc[pos_token, move] if q_tab.index.contains(
                    pos_token) else worst_score) + abs_worst

    title = "Best" if move is None else move.name
    title = title_prefix + title

    plot_heatmap(world_map, q_heatmap, start_pos, title)


def plot_path(world_map: np.ndarray, start_pos: Tuple[int, int], path: List[Tuple[int, int]], title: str):
    heatmap = np.zeros(world_map.shape)
    for i, pos in enumerate(path):
        y, x = pos
        heatmap[y, x] = i
    plot_heatmap(world_map, heatmap, start_pos, title)


def plot_heatmap(world_map: np.ndarray, heatmap: np.ndarray, start_pos: Tuple[int, int], title: str):
    tmp = world_map[start_pos[0], start_pos[1]]
    world_map[start_pos[0], start_pos[1]] = FieldType.PLAYER.value

    cmap = mcolors.ListedColormap(['white', 'red', 'blue', 'green', 'pink', 'orange','cyan'])

    im = plt.imshow(world_map, cmap=cmap)
    fields_on_map = seq([f for f in FieldType]).filter(lambda f: (world_map == f.value).sum() > 0).to_list()
    field_colors = {field_type: im.cmap(im.norm(field_type.value)) for field_type in fields_on_map}
    patches = [mpatches.Patch(color=color, label=field_type.name) for field_type, color in field_colors.items()]
    plt.legend(handles=patches, bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)

    plt.imshow(heatmap, alpha=0.5, cmap="gray_r")

    plt.title(title)
    plt.savefig("tmp/jpegs/" + title.replace(" ", "_") + ".jpg")
    plt.show()
    world_map[start_pos[0], start_pos[1]] = tmp
