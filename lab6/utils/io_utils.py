import pickle
from typing import Union, Tuple, Optional

import numpy as np


def save_array(name: str, array: np.ndarray):
    np.save("tmp/{}".format(name), array)


def load_array(name):
    return np.load("tmp/{}.npy".format(name))


def dump(x, filename):
    file = open("tmp/" + filename, "wb")
    pickle.dump(x, file)
    file.close()


def load(filename):
    file = open("tmp/" + filename, "rb")
    res = pickle.load(file)
    file.close()
    return res


def tokenize_state(state: Union[Tuple[int, int], str]) -> str:
    return state if type(state) == str else "{}|{}".format(state[0], state[1])


def detokenize_state(state: str) -> Optional[Tuple[int, int]]:
    if "|" in state:
        y, x = state.split("|")
        return int(y), int(x)
    else:
        return None


def load_map_start_monster():
    return load_array("world_map"), load("start_pos"), load("monster_pos")


def save_map_start_monster(map_: np.ndarray, start: Tuple[int, int], monster: Tuple[int, int]):
    save_array("world_map", map_)
    dump(start, "start_pos")
    dump(monster, "monster_pos")
