from enum import Enum


class FieldType(Enum):
    EMPTY = 0
    MOUNTAIN = 1
    MONSTER = 2
    PLAYER = 3
    LAVA = 4
    TRISS = 5
    SHANI= 6

    def __str__(self):
        return self.name

    @staticmethod
    def from_name(name):
        return {field.name: field for field in FieldType}[name]

    @staticmethod
    def from_value(value):
        return {field.value: field for field in FieldType}[value]


class Location:
    def __init__(self, x: int, y: int):
        self.x = x
        self.y = y


class Field(Location):
    def __init__(self, x: int, y: int, field_type: FieldType):
        self.field_type = field_type
        super().__init__(x, y)

    def __repr__(self):
        return "Field(x={},y={},type={})".format(self.x, self.y, self.field_type.name)


class Move(Enum):
    UP = 0
    RIGHT = 1
    DOWN = 2
    LEFT = 3

    def __str__(self):
        return self.name

    @staticmethod
    def from_name(name):
        return {field.name: field for field in Move}[name]

    @staticmethod
    def from_value(value):
        return {field.value: field for field in Move}[value]
