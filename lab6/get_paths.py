#!/usr/bin/python3
# based on
# github.com/sichkar-valentyn

import numpy as np

from utils import plots, worldmap_utils as maps
from domain import FieldType, Move
from utils.io_utils import load_map_start_monster, tokenize_state
from qlearn.agent_brain import QLearningTable
from qlearn.tk_env import Environment

STEP_INDEX = 0
REWARD_INDEX = 1
COST_INDEX = 2


def generate_paths(epoch_size: int, start_pos, env: Environment, world_map: np.ndarray,
                   RL: QLearningTable):
    for episode_in_epoch in range(epoch_size):

        observation = env.reset()

        i = 0

        cost = 0
        episode_reward = 0

        action = RL.choose_action(tokenize_state(observation))
        while True:

            if RL.is_sarsa:
                observation_, reward, done = env.step(action)
                action_ = RL.choose_action(tokenize_state(observation_))
                cost += RL.learn(tokenize_state(observation), action, reward,
                                 tokenize_state(observation_), action_)
                action = action_
            else:
                action = RL.choose_action(tokenize_state(observation))
                observation_, reward, done = env.step(action)
                cost += RL.learn(tokenize_state(observation), action, reward,
                                 tokenize_state(observation_), None)

            episode_reward += reward

            # Swapping the observations - current and next
            observation = observation_

            i += 1

            if done:
                break

        path = list(env.current_route.values())
        if episode_in_epoch == 0:
            plots.plot_path(world_map, start_pos, path, title="Initial path")
        elif episode_in_epoch == epoch_size - 1:
            plots.plot_path(world_map, start_pos, path, title="Final path")

    env.final()


def main():
    generate = False

    RL = QLearningTable(actions=[move for move in Move], title="Regular")
    if generate:
        world_map = maps.worldmap_without_unreachable_places((16, 16), {FieldType.EMPTY: 20, FieldType.MOUNTAIN: 30})
        monster_pos = maps.random_pos_on_empty_field(world_map)
        world_map[monster_pos[0], monster_pos[1]] = FieldType.MONSTER.value
        start_pos = maps.random_pos_on_empty_field(world_map)

        tmp = world_map[start_pos[0], start_pos[1]]
        world_map[start_pos[0], start_pos[1]] = FieldType.PLAYER.value
        world_map[start_pos[0], start_pos[1]] = tmp
    else:
        world_map, start_pos, monster_pos = load_map_start_monster()
        print("start : {}".format(start_pos))
        print("monster : {}".format(monster_pos))

    env = Environment(world_map, start_pos, step_limit=700)
    generate_paths(100, start_pos, monster_pos, env, world_map, RL)


if __name__ == '__main__':
    main()
