from typing import Optional, Tuple, Dict
import os

from utils import plots, worldmap_utils as maps, io_utils
from domain import FieldType, Move
import numpy as np

from get_paths import generate_paths
from qlearn.agent_brain import QLearningTable
from qlearn.tk_env import Environment
from run_agent import reward_plots

EPOCH_SIZE = 100  # Todo - undo


def gen_view_save_map(
        shape: Optional[Tuple[int, int]] = None,
        cluster_centers: Optional[Dict[FieldType, int]] = None,
        lava_to_add: Optional[int] = None,
        add_tris: bool = False,
        add_shani: bool = False
):
    shape = (16, 16) if shape is None else shape

    cluster_centers = {FieldType.EMPTY: 20, FieldType.MOUNTAIN: 30} if shape is None else cluster_centers

    if lava_to_add is not None:
        cluster_centers[FieldType.LAVA] = lava_to_add

    world_map = maps.worldmap_without_unreachable_places(shape, cluster_centers)
    start_pos = maps.random_pos_on_empty_field(world_map)
    monster_pos = maps.random_pos_on_empty_field(world_map)
    if add_tris:
        add_friend_to_map(start_pos, world_map, FieldType.TRISS)
    if add_shani:
        add_friend_to_map(start_pos, world_map, FieldType.SHANI)
    world_map[monster_pos[0], monster_pos[1]] = FieldType.MONSTER.value

    plots.plot_heatmap(world_map, np.zeros(shape), start_pos, "Just map presentattion")

    io_utils.save_map_start_monster(world_map, start_pos, monster_pos)


def add_friend_to_map(start_pos: Tuple[int, int], world_map: np.ndarray, field: FieldType):
    near = 3
    added = False
    for y in range(max(0, start_pos[0] - near), min(world_map.shape[0], start_pos[0] + near)):
        if added:
            break
        for x in range(max(0, start_pos[1] - near), min(start_pos[1] + near, world_map.shape[1])):
            if added:
                break
            if world_map[y, x] == FieldType.EMPTY.value:
                added = True
                world_map[y, x] = field.value
    if not added:
        raise Exception("cannot put {} near withcer".format(field.value))


# also saves brain
def gen_save_reward_plots(
        env, brain, world_map, start_pos
):
    reward_plots(epochs=30,
                 epoch_size=EPOCH_SIZE,
                 env=env,
                 RL=brain,
                 world_map=world_map,
                 start_pos=start_pos,
                 animate=False)


def gen_save_paths(
        start_pos, env, world_map, brain
):
    generate_paths(epoch_size=EPOCH_SIZE, start_pos=start_pos, env=env, world_map=world_map, RL=brain)


def gen_save_occurrence_heatmap(
        world_map,
        env,
        brain,
        start_pos,
        monster_pos
):
    from ocurrence_heatmap import generate_occurrence_heatmap

    generate_occurrence_heatmap(world_map=world_map,
                                env=env,
                                brain=brain,
                                start_pos=start_pos,
                                monster_pos=monster_pos,
                                epochs=5,
                                epoch_size=EPOCH_SIZE,
                                generate=False,
                                title="Heatmap without learning")

    # generate_occurrence_heatmap(world_map=world_map,
    #                             env=env,
    #                             brain=brain,
    #                             start_pos=start_pos,
    #                             monster_pos=monster_pos,
    #                             epochs=10,
    #                             epoch_size=100,
    #                             generate=True,
    #                             title="Won't be used")
    brain.load()

    generate_occurrence_heatmap(world_map=world_map,
                                env=env,
                                brain=brain,
                                start_pos=start_pos,
                                monster_pos=monster_pos,
                                epochs=5,
                                epoch_size=EPOCH_SIZE,
                                generate=False,
                                title="Heatmap with learning")


def gen_save_moves(world_map: np.ndarray, start_pos, learned_brain):
    moves = [m for m in Move] + [None]

    for m in moves:
        plots.plot_direction_move(world_map, learned_brain, start_pos, m,
                                  "SARSA " if learned_brain.is_sarsa else "Q-Learn ")


def main():
    world_map, start_pos, monster_pos = io_utils.load_map_start_monster()
    sarsa = True

    def env():
        return Environment(world_map, start_pos, monster_pos=monster_pos, step_limit=100)

    def brain():
        return QLearningTable(
            [move for move in Move],
            "Sarsa" if sarsa else "",
            alpha=0.05,  # learning rate
            gamma=0.8,  # reward_decay
            epsilon=0.8,  # greedy
            is_sarsa=sarsa)

    gen_save_reward_plots(
        env(),
        brain(),
        world_map,
        start_pos
    )

    gen_save_paths(
        start_pos,
        env(),
        world_map,
        brain()
    )

    gen_save_occurrence_heatmap(
        world_map, env(), brain(), start_pos, monster_pos
    )

    learned = brain()
    learned.load()
    learned.print_q_table()
    gen_save_moves(world_map, start_pos, learned)

    os.rename("tmp/jpegs", "tmp/{}".format("sarsa" if sarsa else "q-learn"))
    os.mkdir("tmp/jpegs")


if __name__ == '__main__':
    main()
    # gen_view_save_map(cluster_centers={FieldType.EMPTY: 30, FieldType.MOUNTAIN: 30, FieldType.LAVA: 8}, add_tris=True,
    #                   add_shani=True)
