import math
from typing import Iterator, Union

import numpy as np
from typing_extensions import Final


def generate_points_in_hypercube(center: np.ndarray, edge_len: float, point_count: int) -> Iterator[np.ndarray]:
    dim: Final[int] = center.size
    radius = edge_len / 2

    for i in range(point_count):
        np.random.seed()
        yield np.random.uniform(-radius, radius, dim)


def is_point_in_sphere(center: Union[np.ndarray, None], radius: float, point: np.ndarray) -> bool:
    return np.linalg.norm(
        point if center is None else point - center
    ) < radius


def save_array(name: str, array: np.ndarray, directory: str = "saved") -> None:
    np.save("{}/{}".format(directory, name), array)


def round_to_std_dev(averages: np.ndarray, std_deviation: np.ndarray):
    decimals: np.ndarray = np.ceil(np.log10(std_deviation) * -1) + 1

    result = np.zeros(averages.shape)

    for i in range(averages.size):




        decimal = decimals[i]
        if math.fabs(decimal) != math.inf:
            result[i] = round(averages[i], int(decimal))
        else:
            result[i] = averages[i]
    return result


def plotly_credentials():
    [usr, name] = open("credentials").readlines()
    return usr, name


def load_array(name: str, directory: str = "saved") -> np.ndarray:
    return np.load("{}/{}.npy".format(directory, name))
