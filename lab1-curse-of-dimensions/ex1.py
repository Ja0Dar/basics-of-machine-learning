from typing import List
import numpy as np

from functional import seq
from utils import (generate_points_in_hypercube, is_point_in_sphere,
                   load_array, save_array)


def ex1_result(dims: int, point_count: int) -> np.ndarray:
    result = np.zeros(dims)

    for dim in range(1, dims + 1):
        center = np.zeros(dim, dtype=float)
        points = generate_points_in_hypercube(center, 2, point_count)
        points_in_sphere = seq(points) \
            .count(lambda point: is_point_in_sphere(center=None, radius=1, point=point))

        result[dim - 1] = points_in_sphere / point_count
    return result


def generate_results(test_count: int, dims: int, point_count: int) -> None:
    for i in range(test_count):
        res = ex1_result(dims=dims, point_count=point_count)
        save_array("ex1_{}".format(i), res)
        print("Generated results for {} test, dims={}, point_count={}".format(i, dims, point_count), flush=True)


def load_results(test_count) -> List[np.ndarray]:
    return [load_array("ex1_{}".format(i)) for i in range(test_count)]
