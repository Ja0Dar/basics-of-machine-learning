Klątwa wymiarów
---
Celem zadania jest zbadanie, jak w zależności od liczby wymiarów zmieniają się poniższe wartości.
 
 Na potrzeby zadania stosujemy odległość Euklidesa.
  
1. Mamy hiperkulę o promieniu równym X wpisaną w hipersześcian o krawędziach długości 2X.

   Hiperkulę w przestrzeniach wielowymiarowych definiujemy jako zbiór punktów o odległości od jej środka nie większej niż jej promień.
   
    Zapełniamy hipersześcian losowymi punktami o równomiernym rozkładzie.
    
     Jaki % z tych punktów znajdzie się wewnątrz kuli, a jaki na zewnątrz - w "narożnikach"? 

2. Mamy hipersześcian o krawędziach długości 1.
  Zapełniamy go losowymi punktami o równomiernym rozkładzie.
   
   Jaki jest stosunek odchylenia standardowego odległości między tymi punktami do średniej odległości między nimi?

3. Ponownie mamy losowo zapełniony punktami hipersześcian o krawędziach długości 1.
 
 Z tych punktowych losujemy (bez zwracania) dwie pary.
  
  Punkty z pary wyznaczają pewien wektor (są jego początkiem i końcem).
   
   Jaki jest kąt między dwoma wylosowanymi punktami?
    
   Losowanie powtórz wielokrotnie.
   
   Jak wygląda rozkład otrzymanych kątów?
   
> Dla wszystkich podpunktów tworzymy odpowiednie wykresy:
> 
> * liniowy
> * punktowy
> * histogram
> * tzn. co akurat pasuje :)
>  
>dobierając rozsądny zakres badanych wymiarów i liczbę punktów generowanych we wnętrzach sześcianów.
> 
>Wielkości te można wyznaczyć analitycznie, ale zadanie ma również na celu oswojenie ze stochastyczną naturą eksperymentów przeprowadzanych w ramach tego kursu.
> 
>W konsekwencji - wykres powinien przedstawiać średni rezultat z rozsądnej liczby powtórzeń eksperymentu oraz jego odchylenie standardowe, zaprezentowane jako error bars (tylko w ten sposób można wiarygodnie przedstawiać informacje z obserwacji zjawisk losowych). 
>
>Przypominam również o podpisywaniu osi i uwzględnianiu informacji o jednostkach (jeżeli takowe występują). W komentarzu zastanowić się, jakie konsekwencje dla algorytmów rozpoznawania wzorców mają uzyskane wyniki.