from typing import List

from utils import *


def ex2_result(dims: int, point_count: int) -> np.ndarray:
    result = np.zeros(dims)

    for dim in range(1, dims + 1):
        center = np.zeros(dim, dtype=float)
        points = generate_points_in_hypercube(center, 2, point_count)

        distances = np.array(list(points_distances(points)))
        result[dim - 1] = distances.std() / np.average(distances)
    return result


def points_distances(points: Iterator[np.ndarray]) -> Iterator[int]:
    pts: Final[List[np.ndarray]] = list(points)

    for i in range(len(pts)):
        for j in range(i, len(pts)):
            yield np.linalg.norm(pts[i] - pts[j])


def generate_results(test_count: int, dims: int, point_count: int) -> None:
    for i in range(test_count):
        res = ex2_result(dims=dims, point_count=point_count)
        save_array("ex2_{}".format(i), res)
        print("Generated results for {} test, dims={}, point_count={}".format(i, dims, point_count), flush=True)


def load_results(test_count) -> List[np.ndarray]:
    return [load_array("ex2_{}".format(i)) for i in range(test_count)]
