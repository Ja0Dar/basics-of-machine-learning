import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


def pandas_averages(rounded: np.ndarray, std_dev: np.ndarray):
    matrix = {
        'Dimension': np.arange(rounded.size) + 1,
        'Average': rounded,
        'Std deviation': std_dev}
    return pd.DataFrame(matrix)


def plot_std_dev(rounded: np.ndarray, std_dev: np.ndarray, ylabel, title):
    x = np.arange(rounded.size, dtype=int) + 1
    y = rounded
    yerr = std_dev

    plt.figure()
    plt.xlabel("Dimensions")
    plt.ylabel(ylabel)
    plt.errorbar(x, y, yerr=yerr, barsabove=True, fmt='--.')
    plt.title(title)
    plt.show()
