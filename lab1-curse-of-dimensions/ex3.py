from typing import Tuple, List
import matplotlib.pyplot as plt
import numpy as np
import math
from utils import save_array, round_to_std_dev, load_array


def generate_results(dims: int, cube_edge: int, test_count: int) -> None:
    radius = cube_edge / 2

    for dim in range(1, dims + 1):
        test_result = np.zeros(test_count)
        for i in range(test_count):
            p1, p2, p3, p4 = (np.random.uniform(-radius, radius, dim)
                              for i in range(4))
            vec1 = p2 - p1
            vec2 = p4 - p3
            test_result[i] = angle_between_vecs(vec1, vec2)
        save_array("ex3_dim{}".format(dim), test_result)
        print("Generated info for dim {}, test_count={}".format(
            dim, test_count))


def angle_between_vecs(vec1: np.ndarray, vec2: np.ndarray):
    vec1 = vec1 / np.linalg.norm(vec1)
    vec2 = vec2 / np.linalg.norm(vec2)

    vstack = np.vstack((vec1, vec2))
    scalar_prod = np.prod(vstack, 0).sum()
    # norms = np.linalg.norm(vec1) * np.linalg.norm(vec2)
    return math.acos(np.clip(scalar_prod, -1, 1))


def avg_rounded_std_dev(results: List[np.ndarray]
                        ) -> Tuple[np.ndarray, np.ndarray, np.ndarray]:
    res_arr = np.array(results)
    avgs = np.average(res_arr, 1)
    std_dev = np.std(res_arr, 1)
    rounded = round_to_std_dev(avgs, std_dev)

    return avgs, rounded, std_dev


def load_arrays(dims: int) -> List[np.ndarray]:
    return [load_array("ex3_dim{}".format(i + 1)) for i in range(dims)]


def plot_ex3(results: np.ndarray, dim: int):
    x = results
    plt.hist(x, 70, normed=1, facecolor='green', alpha=0.75)

    plt.xlabel('Angle (radians)')
    plt.ylabel('Number of angles in bucket')
    plt.title('Distribution of angles between random vectors in {} dimensions'.
              format(dim))
    plt.grid(True)
    plt.axis([0, 3.3, 0, 3])
    plt.show()


if __name__ == '__main__':
    pass
