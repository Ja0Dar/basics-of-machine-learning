import os
from typing import Tuple

import cv2

cascPath = "haarcascade_frontalface_default.xml"
faceCascade = cv2.CascadeClassifier(cascPath)


def prepare_face(source_img_fn: str, dst_im_fn: str, shape: Tuple[int, int]) -> None:
    image = cv2.imread(source_img_fn)

    if image is None:
        print(source_img_fn)
        return

    if image.size <= 2:
        print(source_img_fn)
        return

    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    min_size = int(image.size[0]/10)
    faces = faceCascade.detectMultiScale(
        gray,
        scaleFactor=1.1,
        minNeighbors=5,
        minSize=(min_size, min_size)
        # flags = cv2.CV_HAAR_SCALE_IMAGE
    )

    if len(faces) != 1:
        print("O! detected {} faces in image {}".format(len(faces), source_img_fn))

    i = 0
    for (x, y, w, h) in faces:
        x = max(0, int(x - w * 0.1))
        y = max(0, int(y - h * 0.1))
        w = int(w * 1.2)
        h = int(h * 1.2)
        cropped = cv2.resize(gray[y:y + h, x:x + w], shape)
        cv2.imwrite(dst_im_fn + str(i) + ".jpg", cropped)
        i += 1


def prepare_faces(home_dir: str):
    dst_shape = (128, 128)
    base_dir = home_dir + "/Nextcloud/tmp_photos"
    faces_dir = home_dir + "/Nextcloud/tmp_faces"
    try:
        os.mkdir(faces_dir)
        print("Created {}".format(faces_dir))
    except FileExistsError:
        print("Skipping creating {}".format(faces_dir))

    for directory in os.listdir(base_dir):
        cur_dir = base_dir + "/" + directory
        cur_faces_dir = faces_dir + "/" + directory
        try:
            os.mkdir(cur_faces_dir)
            for file in os.listdir(cur_dir):
                prepare_face(cur_dir + "/" + file, cur_faces_dir + "/" + file, dst_shape)
            print("Finished preparing faces for dir {}".format(directory))
        except FileExistsError:
            print("Dir {} exists, skipping extracting faces for it".format(directory))


if __name__ == '__main__':
    prepare_faces("/Users/jakubdarul")
