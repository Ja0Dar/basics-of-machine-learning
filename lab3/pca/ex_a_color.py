from enum import Enum


class ExAColor(Enum):
    PURPLE = 1
    BLUE = 2
    GREEN = 3
    YELLOW = 4

    def __str__(self):
        return self.name

    @staticmethod
    def from_name(name):
        return {color.name: color for color in ExAColor}[name]

    @staticmethod
    def from_value(value):
        return {color.value: color for color in ExAColor}[value]
