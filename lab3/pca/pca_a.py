from typing import List

import numpy as np
import matplotlib.patches as mpatches
from matplotlib import gridspec
from sklearn.decomposition import PCA
from image_generation import generate_colorful_points
from lab2.lab2_point import Color
import matplotlib.pyplot as plt


def reduce_dim(X: np.ndarray, dims: int):
    pca = PCA(n_components=dims)
    pca.fit(X)
    return pca.transform(X)


def plot_ex1_a(dims: List[int]):
    legend = {
        Color.PURPLE: "Corners",
        Color.YELLOW: "Edges",
        Color.GREEN: "Inside sphere",
        Color.BLUE: "Outside sphere"
    }

    handles = [mpatches.Patch(color=color.name.lower(), label=label) for color, label in legend.items()]

    for dim in dims:
        gs = gridspec.GridSpec(1, 2)
        gs.update(hspace=0.4)

        x_, y = generate_colorful_points(dim, int(1e5))
        fig = plt.figure()
        fig.set_size_inches(18,10)

        ax2d = fig.add_subplot(gs[0, 0])
        ax2d.set_title("Projection {}d to 2d".format(dim))
        ax2d.legend(handles=handles)
        projection_2d = reduce_dim(x_, 2)
        ax2d.scatter(projection_2d[:, 0], projection_2d[:, 1], c=y, s=1)

        projection_3d = reduce_dim(x_, 3)
        ax3d = fig.add_subplot(gs[0, 1], projection='3d')
        ax3d.set_title("Projection {}d to 3d".format(dim))
        ax3d.legend(handles=handles)
        ax3d.scatter(projection_3d[:, 0], projection_3d[:, 1], projection_3d[:, 2], c=y, s=1)
        plt.show()
