# A: Wróćmy do hiperkuli wpisanej w hipersześcian z pierwszego zadania.
#
#
# Pokolorujmy :
# * na czubkach narożników sześcianu na czerwono,
# * punkty na krawędziach na żółto (mogą wymagać dodatkowej generacji, bo w losowym rozkładzie się raczej nie pojawią),
# *  punkty w jego wnętrzu (ale nie we wnętrzu kuli) na niebiesko,
# *  a punkty z kuli na zielono.
#
# Wykorzystać metodę PCA by wykonać wizualizację (rzut na płaszczyznę 2D) tejże sytuacji dla 3, 4, 5, 7 i 13 wymiarów. Powtórzyć to samo, ale dla wizualizacji 3D. Krótko opisać co widać. ;]
from typing import Callable, Tuple

from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import matplotlib.pyplot as plt

# all centered at 0,0
import random

import plot
from ex_a_color import ExAColor


def points_inisde_hipercube(radius: float, dims: int, point_count: int) -> np.ndarray:
    shape = (point_count, dims)
    rad = np.float32(radius)
    return np.random.uniform(-rad, rad, shape)


def random_float(min_value: float, max_value: float) -> float:
    return min_value + random.random() * (max_value - min_value)


def dist_from_0(X: np.ndarray) -> np.ndarray:
    return np.linalg.norm(np.zeros(X.shape, dtype=np.float32) - X, axis=1)


def indices_inside_hipersquere(radius: float, norm: Callable[[np.ndarray], np.ndarray], X: np.ndarray):
    return norm(X) < np.float32(radius)


def points_on_hipersquare_borders(radius: float, dims: int, point_count) -> np.ndarray:
    res = points_or_hipersquare_corners(radius, dims, point_count)
    for row in res:
        row[random.randint(0, dims - 1)] = np.float32(random_float(-radius, radius))
    return res


def points_or_hipersquare_corners(radius: float, dims: int, point_count) -> np.ndarray:
    rad = np.float32(radius)
    shape = (point_count, dims)
    return np.random.choice([-rad, rad], shape)


def generate_colorful_points(dims: int, point_count: int) -> Tuple[np.ndarray, np.ndarray]:
    radius = 1.0
    arr = points_inisde_hipercube(radius, dims, point_count)
    inside_indices = indices_inside_hipersquere(radius, dist_from_0, arr)

    arr_colors = np.zeros((point_count, 1), dtype=float)
    arr_colors[:] = ExAColor.BLUE.value
    arr_colors[inside_indices] = ExAColor.GREEN.value

    corner_points = point_count // 20
    corners = points_or_hipersquare_corners(radius, dims, corner_points)
    corner_colors = np.zeros((corner_points, 1), dtype=float)
    corner_colors[:] = ExAColor.PURPLE.value

    border_points = point_count // 8
    borders = points_on_hipersquare_borders(radius, dims, border_points)
    border_colors = np.zeros((border_points, 1), dtype=float)
    border_colors[:] = ExAColor.YELLOW.value

    points = np.vstack((arr, corners, borders))
    colors = np.vstack((arr_colors, corner_colors, border_colors))
    colors = colors.reshape((colors.shape[0],))

    return points, colors


if __name__ == '__main__':
    x_, y_ = generate_colorful_points(3, int(1e4))
    plot.plot_scatter_3d(x_, y_)
