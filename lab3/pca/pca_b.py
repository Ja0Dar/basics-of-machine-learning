import math
from typing import Dict

from functional import seq
from fn import _
from matplotlib import gridspec
from sklearn.decomposition import PCA, KernelPCA
import matplotlib.pyplot as plt
import numpy as np
from lab2 import img_utils, lab2_point
from lab2.lab2_point import Color
from plot import  plot_vecs


# todo:bcm - use kernel pca


def ex1_b_vec(ax, img: np.ndarray):
    x_, y_ = img_utils.get_X_y(img_utils.get_points(img))

    ax.scatter(x_[:, 0], x_[:, 1], c=y_, s=1)

    for color in Color:
        inds = y_ == color.value
        class_ = x_[inds]
        plot_vecs(ax, class_, color)

    ax.set_aspect('equal')


def ex1_b_projected(ax, img: np.ndarray):
    x_, y_ = img_utils.get_X_y(img_utils.get_points(img))

    classes = dict()
    for color in Color:
        inds = y_ == color.value
        class_ = x_[inds]
        pca = PCA(n_components=2)
        pca.fit(class_)
        transformed = pca.transform(class_)
        classes[color] = transformed

    xs_ys = [(xs, np.ones(xs.shape[0]) * color.value) for color, xs in classes.items()]

    x_ = np.vstack(seq(xs_ys).map(lambda tup: tup[0]).to_list())
    y_ = np.vstack(seq(xs_ys).map(lambda tup: tup[1].reshape(-1, 1)).to_list()).reshape((-1,))

    ax.scatter(x_[:, 0], x_[:, 1], c=y_, s=1)


def ex1_b_projected_kernel(ax, img: np.ndarray, kpca: KernelPCA, title="", recenter: bool = False):
    x_, y_ = img_utils.get_X_y(img_utils.get_points(img))
    if recenter:
        mu = x_.mean(axis=0)
        x_ = x_ - mu

    classes = dict()
    for color in Color:
        inds = y_ == color.value
        class_ = x_[inds]
        kpca.fit(class_)
        transformed = kpca.transform(class_)
        classes[color] = transformed

    xs_ys = [(xs, np.ones(xs.shape[0]) * color.value) for color, xs in classes.items()]

    x_ = np.vstack(seq(xs_ys).map(lambda tup: tup[0]).to_list())
    y_ = np.vstack(seq(xs_ys).map(lambda tup: tup[1].reshape(-1, 1)).to_list()).reshape((-1,))
    ax.set_title(title)
    ax.scatter(x_[:, 0], x_[:, 1], c=y_, s=1)


def ex_b_2_kernel_trick(img: np.ndarray, pcas: Dict[KernelPCA, str], cols: int = 2) -> None:
    rows = math.ceil(len(pcas) / cols)

    gs = gridspec.GridSpec(rows, cols)
    gs.update(hspace=0.4)

    fig = plt.figure()
    fig.set_size_inches(18, rows * 10)

    for i, tup in enumerate(pcas.items()):
        kpca, label = tup
        if 'recenter_points' in kpca.__dict__:
            print('recentering')
            recenter = kpca.recenter_points
        else:
            recenter = False
        ax = fig.add_subplot(gs[i // cols, i % cols])
        ex1_b_projected_kernel(ax, img, kpca, label, recenter)


def ex_b_1():
    gs = gridspec.GridSpec(1, 2)
    gs.update(hspace=0.4)

    fig = plt.figure()
    fig.set_size_inches(18, 10)

    ax_vecs = fig.add_subplot(gs[0, 0])
    img = img_utils.load_and_preprocess_all(["pca.bmp"])[0]
    ex1_b_vec(ax_vecs, img)
    ax_pca = fig.add_subplot(gs[0, 1])
    ex1_b_projected(ax_pca, img)

    plt.show()

    # ex1_b_projected_kernel(img, KernelPCA(n_components=2, kernel='rbf', gamma=0.001))


if __name__ == '__main__':
    ex_b_1()
