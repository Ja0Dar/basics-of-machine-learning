import random

import numpy as np
import matplotlib.pyplot as plt

from lab2.lab2_point import Color


def plot_vecs(ax, data: np.ndarray, color: Color, move_randomly: bool = True) -> None:
    mu = data.mean(axis=0)
    centered_data = data - mu

    if move_randomly:
        dx = random.randint(-50, 50)
        dy = random.randint(-50, 50)
    else:
        dx, dy = 0, 0
    dp = np.array([dy, dx])

    eigenvectors, eigenvalues, V = np.linalg.svd(data.T, full_matrices=False)
    projected_data = np.dot(centered_data, eigenvectors)
    sigma = projected_data.std(axis=0).mean()  # todo:bcm - why this?

    for axis in eigenvectors:
        start, end = dp + mu, dp + mu + sigma * axis
        ax.annotate(
            '', xy=end, xycoords='data',
            xytext=start, textcoords='data',
            arrowprops=dict(facecolor=color.name.lower(), width=2.0))


def plot_scatter_2d(X: np.ndarray, y: np.ndarray):
    plt.scatter(X[:, 0], X[:, 1], c=y, s=1)
    plt.xticks(())
    plt.yticks(())
    plt.axes()
    plt.show()


def plot_scatter_3d(X: np.ndarray, y: np.ndarray):
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')

    ax.scatter(X[:, 0], X[:, 1], X[:, 2], c=y, s=1)
    plt.show()
