import random
from typing import List, Tuple, Callable, Optional

import numpy as np
import sklearn.neighbors as nb

import img_utils
import sklearn_utils
from img_utils import get_X_y
from point import Point


def add_noise(img_shape: Tuple[int, int], points: List[Point], radius: float) -> List[Point]:
    for point in points:
        point.x = np.clip(point.x + img_utils.random_float(-radius, +radius), 0, img_shape[1])
        point.y = np.clip(point.y + img_utils.random_float(-radius, +radius), 0, img_shape[0])
    return points


def get_accuracy_sklearn(model: nb.KNeighborsClassifier, test_points: List[Point]) -> float:
    X, actual_y = get_X_y(test_points)

    predicted_y = model.predict(X)

    correct_predictions = np.sum(predicted_y == actual_y)

    res = correct_predictions.sum() / len(test_points)
    return res


def get_accuracy_sklearn_avg(
        classifier_provider: Callable[
            [List[Point]], Tuple[nb.KNeighborsClassifier, Optional[float], Optional[List[Point]]]],
        points_: List[Point],
        repeats: int = 10,
        train_percentage: float = 0.7,
) -> Tuple[float, float, float, float]:
    cutoff = int(train_percentage * len(points_))
    results = []
    cnn_compression_results = []

    for _ in range(repeats):
        random.shuffle(points_)
        train = points_[:cutoff]
        test = points_[cutoff:]

        clf, cnn_compression, _ = classifier_provider(train)
        if cnn_compression is not None:
            cnn_compression_results.append(cnn_compression)

        results.append(
            get_accuracy_sklearn(clf, test)
        )

    if len(cnn_compression_results) == 0:
        rounded_cnn_avg, rounded_cnn_std_dev = None, None
    else:
        cnn_avg = np.average(cnn_compression_results)
        cnn_std = np.std(cnn_compression_results)
        rounded_cnn_avg = sklearn_utils.round_to_std_dev(np.array([cnn_avg]), np.array([cnn_std]))[0]
        rounded_cnn_std_dev = sklearn_utils.round_st_dev(cnn_std)

    avg = np.average(results)
    std = np.std(results)

    rounded_avg = sklearn_utils.round_to_std_dev(np.array([avg]), np.array([std]))
    rounded_std_dev = sklearn_utils.round_st_dev(std)
    return rounded_avg[0], rounded_std_dev, rounded_cnn_avg, rounded_cnn_std_dev
