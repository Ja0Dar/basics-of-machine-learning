import math
from typing import Callable, List

import matplotlib.pyplot as pl
import numpy as np
from sklearn import neighbors

import knn_sklearn
from point import Point
from sklearn_mahalanobis import MahalanobisParial


# plotting mesh from
# http://ogrisel.github.io/scikit-learn.org/sklearn-tutorial/auto_examples/tutorial/plot_knn_iris.html


def fit_and_plot(knn: neighbors.KNeighborsClassifier, X: np.ndarray, Y: np.ndarray, title="", h=.02):
    knn.fit(X, Y)
    plot(knn.predict, X, Y, title, h)


def fit_and_plot_mahalanobis_area(knn: MahalanobisParial, X: np.ndarray, Y: np.ndarray, title="", h=.02):
    plot(knn.predict, X, Y, title, h)


def plot(predictor: Callable[[np.ndarray], np.ndarray], X: np.ndarray, Y: np.ndarray, title: str, h: float):
    x_min, x_max = X[:, 0].min() - .5, X[:, 0].max() + .5
    y_min, y_max = X[:, 1].min() - .5, X[:, 1].max() + .5

    xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))
    Z = predictor(np.c_[xx.ravel(), yy.ravel()])

    # Put the result into a color plot
    Z = Z.reshape(xx.shape)
    pl.figure(1, figsize=(4, 3))
    pl.pcolormesh(xx, yy, Z, cmap=pl.cm.Set2)

    pl.scatter(X[:, 0], X[:, 1], c=Y, cmap=pl.cm.Dark2, s=1)

    pl.title(title)
    pl.xlim(xx.min(), xx.max())
    pl.ylim(yy.min(), yy.max())
    pl.xticks(())
    pl.yticks(())

    pl.show()


def plot_just_points(points: List[Point], title=""):
    X, Y = knn_sklearn.get_X_y(points)
    pl.scatter(X[:, 0], X[:, 1], c=Y, cmap=pl.cm.Dark2, s=1)

    pl.title(title)
    pl.xticks(())
    pl.yticks(())

    pl.show()


def round_st_dev(st_dev: float) -> float:
    decimals: np.ndarray = np.ceil(np.log10(st_dev) * -1) + 1
    return round(st_dev, int(decimals))


def round_to_std_dev(averages: np.ndarray, std_deviation: np.ndarray):
    decimals: np.ndarray = np.ceil(np.log10(std_deviation) * -1) + 1

    result = np.zeros(averages.shape)

    for i in range(averages.size):

        decimal = decimals[i]
        if math.fabs(decimal) != math.inf:
            result[i] = round(averages[i], int(decimal))
        else:
            result[i] = averages[i]
    return result
