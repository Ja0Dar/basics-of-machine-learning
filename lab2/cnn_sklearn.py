import math
import random
from typing import List, Tuple, Dict, Optional

import sklearn
from fn import _
from functional import seq

import img_utils
import knn_sklearn
import sklearn_utils
from point import Point, Color, Location


def euclidean_metric(l1: Location, l2: Location) -> float:
    return math.sqrt(math.pow(l1.x - l2.x, 2) + math.pow(l1.y - l2.y, 2))


class ClosestDists:
    def __init__(self, points: List[Point]):
        for i, point in enumerate(points):
            point.id = i

        self.points = points

        self.closests: Dict[int, Dict[Color, Optional[Tuple[Point, float]]]] = dict()
        colors: Dict[Color, seq[Point]] = {
            color: seq(points).filter(_.color == color) for color in Color
        }

        for point in points:
            closest_point_nghtb_per_color: Dict[Color, Tuple[Point, float]] = dict()
            for color, colored_points in colors.items():

                if colored_points.size() == 0:
                    closest = None
                else:
                    closest = colored_points.min_by(lambda pt: euclidean_metric(point, pt))

                closest_point_nghtb_per_color[color] = None if closest is None else (
                    closest, euclidean_metric(point, closest))

            self.closests[point.id] = closest_point_nghtb_per_color

    def closest_point_with_color(self, from_: Point, color: Color) -> Optional[Tuple[Point, float]]:
        return self.closests[from_.id][color]

    def closest_point_with_other_color(self, point: Point):

        return min((

            self.__none_to_inf(self.closest_point_with_color(point, color))

            for color in Color if color is not point.color
        ), key=lambda tup: tup[1])

    def border_ratio(self, point: Point) -> float:
        y, dist_to_y = self.closest_point_with_other_color(point)
        x2, dist_from_y_to_x2 = self.closest_point_with_color(y, point.color)
        return dist_from_y_to_x2 / dist_to_y

    @staticmethod
    def __none_to_inf(x: Optional[Tuple[Point, float]]) -> Tuple[Point, float]:
        return (None, math.inf) if x is None else x


def cnn(store: List[Point], garbage: List[Point], k: int,
        closest_distances: Optional[ClosestDists] = None, depth: int = 0) -> List[Point]:
    point_was_stored = False
    new_garbage = []
    if depth == 0 and (closest_distances is not None):
        garbage = sorted(garbage, key=lambda pt: closest_distances.border_ratio(pt), reverse=True)

    knn = sklearn.neighbors.KNeighborsClassifier(k, weights='uniform', metric='euclidean')

    X, y = knn_sklearn.get_X_y(store)
    knn.fit(X, y)

    for point in garbage:
        predicted = int(knn.predict(point.vector().reshape(1, -1))[0])
        actual = point.color.value
        if predicted == actual:
            new_garbage.append(point)
        else:
            store.append(point)
            point_was_stored = True
            X, y = knn_sklearn.get_X_y(store)
            knn.fit(X, y)

    if point_was_stored:
        return cnn(store, new_garbage, k, closest_distances, depth + 1)
    else:
        return store


if __name__ == '__main__':
    image = img_utils.load_and_preprocess_all(["small_face.bmp"])[0]
    points_ = knn_sklearn.add_noise(image.shape, img_utils.get_points(image), radius=0.5)
    print(len(points_))
    sklearn_utils.plot_just_points(points_)
    random.shuffle(points_)

    closest_dists = ClosestDists(points_)

    random.shuffle(points_)
    new_store = cnn(points_[:3], points_[3:], k=3, closest_distances=closest_dists)

    sklearn_utils.plot_just_points(new_store)
    print(len(new_store))
