from enum import Enum

import numpy as np


class Color(Enum):
    RED = 1
    BLUE = 2
    GREEN = 3

    def pixel(self) -> np.ndarray:
        if self == Color.RED:
            return np.array([255, 0, 0], dtype=int)
        elif self == Color.GREEN:
            return np.array([0, 255, 0], dtype=int)
        elif self == Color.BLUE:
            return np.array([0, 0, 255], dtype=int)
        else:
            raise Exception("Color has non-rgb value")

    def lighter_pixel(self):
        result = self.pixel()
        result += 180
        return np.clip(result, 0, 255)

    def __str__(self):
        return self.name

    @staticmethod
    def from_name(name):
        return {color.name: color for color in Color}[name]

    @staticmethod
    def from_value(value):
        return {color.value: color for color in Color}[value]


class Location:
    def __init__(self, x: float, y: float):
        if x < 0 or y < 0:
            raise Exception("Point shouldn't have negative values")

        self.x = x
        self.y = y

    def vector(self) -> np.ndarray:
        return np.array([self.x, self.y])


class Point(Location):
    def __init__(self, x: float, y: float, color: Color):
        super().__init__(x, y)
        self.color = color

    def __str__(self) -> str:
        return "Point(x={}, y={}, color={}".format(self.x, self.y, self.color)

    def __repr__(self):
        return str(self)

    def to_string_key(self):
        return "{}_{}_{}".format(self.x, self.y, self.color.name)

    @staticmethod
    def from_string_key(key: str):
        x, y, color = key.split("_")
        return Point(float(x), float(y), Color.from_name(color))

    def __eq__(self, o: object) -> bool:
        return isinstance(o, Point) and self.x == o.x and self.y == o.y and self.color == o.color
