import random
from typing import List, Callable

import matplotlib.image as mpimg
import matplotlib.pyplot as plt
import numpy as np

from point import Point, Color


def recalibrate(pixel: np.ndarray):
    if (pixel > 200).sum() >= 3:
        return np.array([255, 255, 255], dtype=int)
    else:
        max_ind = np.argmax(pixel)
        result = np.zeros(3, dtype=int)
        result[max_ind] = 255
        return result


def recalibrate_row(row: np.ndarray) -> np.ndarray:
    return np.array([recalibrate(pixel) for pixel in row])


def recalibrate_img(img: np.ndarray) -> np.ndarray:
    return np.array([recalibrate_row(row) for row in img])


def load_and_preprocess(image_file: str) -> np.ndarray:
    return recalibrate_img(
        mpimg.imread(image_file)
    )


def load_and_preprocess_all(files: List[str]):
    return [load_and_preprocess(("pics/{}".format(image_fn))) for image_fn in files]


def _get_color(pixel: np.ndarray):
    if (pixel == 255).sum() == 3:
        return None
    else:
        color_num = np.argmax(pixel)
        if color_num == 0:
            return Color.RED
        elif color_num == 1:
            return Color.GREEN
        elif color_num == 2:
            return Color.BLUE
        else:
            raise Exception("Color issue in _get_color, pixel: {}, color_num: {}".format(pixel, color_num))


def get_points(img: np.ndarray) -> List[Point]:
    rows, cols, _ = img.shape

    result = list()
    for i in range(rows):
        for j in range(cols):
            color = _get_color(img[i, j])

            if color is not None:
                result.append(Point(i, j, color))

    return result


def points_to_array(rows: int, cols: int, points: List[Point],
                    color_filler: Callable[[Color], np.ndarray], fill=255) -> np.ndarray:
    result = np.zeros((rows, cols, 3), dtype=int)
    result.fill(fill)
    for point in points:
        result[point.x, point.y] = color_filler(point.color)
    return result


def points_to_color_enum_array(rows: int, cols: int, points: List[Point]) -> np.ndarray:
    result = np.zeros((rows, cols), dtype=object)
    result.fill(None)

    for point in points:
        result[point.x, point.y] = point.color

    return result


def random_float(min_value: float, max_value: float) -> float:
    return min_value + random.random() * (max_value - min_value)


def get_X_y(points: List[Point]):
    y = np.zeros(len(points))
    X = np.zeros((len(points), 2))
    for i, pt in enumerate(points):
        X[i][0] = pt.x
        X[i][1] = pt.y
        y[i] = pt.color.value
    return X, y


if __name__ == '__main__':
    image = load_and_preprocess_all(["small.bmp"])[0]
    plt.imshow(image)
    plt.show()

    points = get_points(image)

    image2 = points_to_array(200, 200, points, lambda x: x.lighter_pixel())

    plt.imshow(image2)
    plt.show()
