import random
from functools import lru_cache
from typing import Tuple, List

import numpy as np
import sklearn.neighbors as nb
from fn import _
from functional import seq

import sklearn_utils
from img_utils import get_X_y
from point import Location, Point, Color


class MahalanobisParial:
    def __init__(self, k: int, points: List[Point], shape: Tuple[float, float], grid_rows: int = 5, grid_cols: int = 5):
        self.k = k
        self.grid_rows = grid_rows
        self.grid_cols = grid_cols
        self.points = seq(points)
        self.min_x = 0  # self.points.map(_.x).min()
        self.max_x = shape[1] + 10.  # self.points.map(_.x).max()
        self.width = self.max_x - self.min_x

        self.min_y = 0  # self.points.map(_.y).min()
        self.max_y = shape[0] + 10.  # self.points.map(_.y).max()
        self.height = self.max_y - self.min_y

        self.grid_row_size = self.height / grid_rows
        self.grid_col_size = self.width / grid_cols

        self.grid_buckets: List[List[List[Point]]] = [
            [
                [] for x in range(grid_cols)
            ] for y in range(grid_rows)
        ]
        for pt in points:
            row, col = self._grid_row_col_location(pt)
            self.grid_buckets[row][col].append(pt)

    def _grid_row_col_location(self, point: Location) -> Tuple[int, int]:
        return int(point.y / self.grid_row_size), int(point.x / self.grid_col_size)

    def _grid_row_col_tuple(self, loc: Tuple[float, float]) -> Tuple[int, int]:
        return int((loc[0] - self.min_y) / self.grid_row_size), int((loc[1] - self.min_x) / self.grid_col_size)

    @staticmethod
    def _cov_for_region(points_in_area: List[Point]):
        arr = np.array([p.vector() for p in points_in_area])
        cov = np.cov(arr.transpose())
        return cov

    @lru_cache(maxsize=60)
    def _points_from_grid_nghb_subareas(self, grid_row: int, grid_col) -> List[Point]:
        pts: List[Point] = []
        for c_row in range(max(0, grid_row - 1), min(self.grid_rows, grid_row + 2)):
            for c_col in range(max(0, grid_col - 1), min(self.grid_cols, grid_col + 2)):
                pts = pts + self.grid_buckets[c_row][c_col]
        return pts

    @lru_cache(maxsize=60)
    def classifier_per_region(self, k, grid_row: int, grid_col: int) -> nb.KNeighborsClassifier:
        points_in_area = self._points_from_grid_nghb_subareas(grid_row, grid_col)
        classifier = nb.KNeighborsClassifier(k, algorithm="brute", metric='mahalanobis',
                                             metric_params={"V": self._cov_for_region(points_in_area)})
        X, y = get_X_y(points_in_area)
        classifier.fit(X, y)
        return classifier

    def knn(self, k: int):
        def inner(location: Location) -> int:
            grid_row, grid_col = self._grid_row_col_location(location)
            classifier = self.classifier_per_region(k, grid_row, grid_col)
            return Color.from_value(classifier.predict(location.vector()))

        return inner

    def predict(self, X: np.ndarray):  # todo - group points from each bucket?
        y = np.zeros(X.shape[0])
        for i, row in enumerate(X):
            grid_row, grid_col = self._grid_row_col_tuple(row)
            classifier = self.classifier_per_region(self.k, grid_row, grid_col)
            y[i] = classifier.predict(row.reshape(1, -1))
        return y


def get_accuracy_mahalanobis_area_avg(k: int, points_: List[Point], shape: Tuple[float, float], grid_rows: int = 5,
                                      grid_cols: int = 5, repeats: int = 10):
    results = np.array(
        [get_accuracy_mahalanobis_area(k, points_, shape, grid_rows, grid_cols) for _ in range(repeats)])

    avg = np.average(results)
    std = np.std(results)

    rounded_avg = sklearn_utils.round_to_std_dev(np.array([avg]), np.array([std]))
    rounded_std_dev = sklearn_utils.round_st_dev(std)
    return rounded_avg[0], rounded_std_dev


def get_accuracy_mahalanobis_area(k: int, points_: List[Point], shape: Tuple[float, float], grid_rows: int = 5,
                                  grid_cols: int = 5) -> float:
    random.shuffle(points_)
    cutoff = int(0.7 * len(points_))
    train = points_[:cutoff]
    test = points_[cutoff:]

    model = MahalanobisParial(k, train, shape, grid_rows, grid_cols)

    X, actual_y = get_X_y(test)

    predicted_y = model.predict(X)

    correct_predictions = np.sum(predicted_y == actual_y)

    return correct_predictions.sum() / len(test)
