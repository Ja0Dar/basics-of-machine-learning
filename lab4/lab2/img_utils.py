import random
import matplotlib.pyplot as plt
from typing import List, Callable, Optional

import matplotlib.image as mpimg
import numpy as np

from lab2.lab2_point import Point, Color


def load_all(files: List[str]) -> List[np.ndarray]:
    return [mpimg.imread("pics/{}".format(image_fn)) for image_fn in files]


def _purple_or_nothing(pixel: np.ndarray) -> Optional[Color]:
    if pixel.sum() < 300:
        return Color.PURPLE
    else:
        return None


def get_points(img: np.ndarray) -> List[Point]:
    rows, cols, _ = img.shape

    result = list()
    for i in range(rows):
        for j in range(cols):
            color = _purple_or_nothing(img[i, j])

            if color is not None:
                result.append(Point(i, j, color))

    return result


def points_to_img_array(rows: int, cols: int, points: List[Point],
                        color_filler: Callable[[Color], np.ndarray], fill=255) -> np.ndarray:
    result = np.zeros((rows, cols, 3), dtype=int)
    result.fill(fill)
    for point in points:
        result[point.x, point.y] = color_filler(point.color)
    return result


def points_to_color_enum_array(rows: int, cols: int, points: List[Point]) -> np.ndarray:
    result = np.zeros((rows, cols), dtype=object)
    result.fill(None)

    for point in points:
        result[point.x, point.y] = point.color
    return result


def random_float(min_value: float, max_value: float) -> float:
    return min_value + random.random() * (max_value - min_value)


def get_X_y(points: List[Point]):
    y = np.zeros(len(points))
    X = np.zeros((len(points), 2))
    for i, pt in enumerate(points):
        X[i][0] = pt.x
        X[i][1] = pt.y
        y[i] = pt.color.value
    return X, y


def plot_clusters(predictor: Callable[[np.ndarray], np.ndarray], X: np.ndarray, Y: np.ndarray, title: str, h: float):
    x_min, x_max = X[:, 0].min() - .5, X[:, 0].max() + .5
    y_min, y_max = X[:, 1].min() - .5, X[:, 1].max() + .5

    xx, yy = np.meshgrid(np.arange(x_min, x_max, h),
                         np.arange(y_min, y_max, h))  # todo- change labels so that they downt overlap
    Z = predictor(np.c_[xx.ravel(), yy.ravel()])

    # Put the result into a color plot
    Z = Z.reshape(xx.shape)
    # todo: get figure and plot on it
    plt.figure(1, figsize=(10, 10))
    plt.pcolormesh(xx, yy, Z, cmap=plt.cm.Set2)

    plt.scatter(X[:, 0], X[:, 1], c=Y, cmap=plt.cm.Dark2, s=1)

    plt.title(title)
    plt.xlim(xx.min(), xx.max())
    plt.ylim(yy.min(), yy.max())

    plt.show()
