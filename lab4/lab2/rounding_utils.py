import math
import numpy as np


def round_st_dev(st_dev: float) -> float:
    decimals: np.ndarray = np.ceil(np.log10(st_dev) * -1) + 1
    return round(st_dev, int(decimals))


def round_means_and_std_devs(means: np.ndarray, std_devs: np.ndarray):
    decimals: np.ndarray = np.ceil(np.log10(std_devs) * -1) + 1
    # todo: roun std_dev to 2 placesgii

    rounded_means = np.zeros(means.shape)
    rounded_std_devs = np.zeros(std_devs.shape)

    for i in range(means.size):

        decimal = decimals[i]
        if math.fabs(decimal) != math.inf:
            rounded_means[i] = round(means[i], int(decimal))
            rounded_std_devs[i] = round(std_devs[i], int(decimal))
        else:
            rounded_means[i] = means[i]
            rounded_std_devs[i] = std_devs[i]

    return rounded_means, rounded_std_devs
